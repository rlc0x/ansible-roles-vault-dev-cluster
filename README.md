ansible-roles-vault-dev-cluster
=========

Installs Hashicorp Vault on RedHat/Rocky servers for development.

**Note:** Do not use this role in production, it uses an insecure method to manage unseal keys at startup.

Requirements
------------

The default configuration calls for three RedHat/Rocky servers with at least one raw disk. 

Role Variables
--------------

**Required Variables:**

vault_primary_node 

The node that will be used to initialize vault.

vault_pv_name 

This disk `/dev/sdb` that will be used for vault storage.

vault_leader_tls_servername

`FQDN` of the **vault_primary_node** or the reverse proxy if using

vault_leader_client_cert_file 

Certificate file created with all the DNS and IP entries as SANs.

vault_leader_client_key_file

Key file

vault_leader_ca_cert_file

CA file

vault_cluster_name default('vault-cluster')

Cluster name


Dependencies
------------


Example Playbook
----------------

    - hosts: vault
      roles:
         - { role: vault-dev-cluster, vault_primary_node: vault1 }

License
-------

MIT

Author Information
------------------

Raymond L. Cox
